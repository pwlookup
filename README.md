These are the scripts behind my little article, [Fast Key Lookup with a Small
Read-Only Database](https://dev.yorhel.nl/doc/pwlookup).

Requirements:

- Perl (64bit!)
- Compress::Zstd
- LMDB_File
- make
