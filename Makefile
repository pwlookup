DICT=crackstation-human-only.txt.gz

all: bench

db/plain: ${DICT}
	mkdir -p db
	zcat ${DICT} | LC_COLLATE=C sort >$@

db/gzip: db/plain
	cat db/plain | gzip >$@

db/lmdb: db/plain
	./lmdb.pl create db/lmdb <db/plain

db/btree-1k-plain: db/plain
	./btree.pl create 1024 plain <db/plain >db/btree-1k-plain

db/btree-4k-plain: db/plain
	./btree.pl create 4096 plain <db/plain >db/btree-4k-plain

db/btree-1k-gzip: db/plain
	./btree.pl create 1024 gzip <db/plain >db/btree-1k-gzip

db/btree-4k-gzip: db/plain
	./btree.pl create 4096 gzip <db/plain >db/btree-4k-gzip

db/btree-1k-zstd: db/plain
	./btree.pl create 1024 zstd <db/plain >db/btree-1k-zstd

db/btree-4k-zstd: db/plain
	./btree.pl create 4096 zstd <db/plain >db/btree-4k-zstd

bench: db/plain db/gzip db/lmdb db/btree-1k-plain db/btree-4k-plain db/btree-1k-gzip db/btree-4k-gzip db/btree-1k-zstd db/btree-4k-zstd
	time -v ./btree.pl bench 1024 plain db/btree-1k-plain
	time -v ./btree.pl bench 4096 plain db/btree-4k-plain
	time -v ./btree.pl bench 1024 gzip  db/btree-1k-gzip
	time -v ./btree.pl bench 4096 gzip  db/btree-4k-gzip
	time -v ./btree.pl bench 1024 zstd  db/btree-1k-zstd
	time -v ./btree.pl bench 4096 zstd  db/btree-4k-zstd
	time -v ./plain.pl db/plain db/gzip
	time -v ./lmdb.pl bench db/lmdb
