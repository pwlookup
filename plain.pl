#!/usr/bin/perl

# Usage: ./plain.pl db-plain db-gzip

use strict;
use warnings;
use PerlIO::gzip;
use v5.10;

sub lookup_str {
    my($F, $q) = @_;
    while(<$F>) {
        chomp;
        return 1 if $q eq $_;
        return 0 if $q lt $_;
    }
    return 0;
}

sub lookup_plain {
    my($q, $f) = @_;
    open my $F, '<', $f or die $!;
    lookup_str $F, $q
}

sub lookup_gzip {
    my($q, $f) = @_;
    open my $F, '<:gzip', $f or die $!;
    lookup_str $F, $q
}

my($plain, $gzip) = @ARGV;

use Benchmark 'timethis', ':hireswallclock';
sub rstr { state $s=['a'..'z','A'..'Z','0'..'9','!','@','#']; join '', map $s->[rand @$s], 1..5 }
srand 0;
timethis 2, sub { lookup_plain rstr(), $plain }, 'plain';
srand 0;
timethis 2, sub { lookup_gzip rstr(), $gzip }, 'gzip';
